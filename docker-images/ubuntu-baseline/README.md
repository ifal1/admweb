Imagem base: 
* ubuntu:focal

Softwares adicionais:
* vim
* net-tools
* iputils-ping
* nmap
* sudo
* iperf
* iproute2
* traceroute
* tcpdump

Configurações adicionais
* Cria usuário padrão 'usuario' com senha 'usuario'
